package com.budgetmaster.exampleproduct.controller;

import com.budgetmaster.exampleproduct.model.MyModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

  @GetMapping("/hello")
  public ResponseEntity<MyModel> hello() {

    MyModel model = new MyModel();
    model.name("name").type("type");

    return ResponseEntity.ok(model);
  }
}
