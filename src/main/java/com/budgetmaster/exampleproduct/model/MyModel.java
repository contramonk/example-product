package com.budgetmaster.exampleproduct.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(fluent = true)
public class MyModel {
  private String name;
  private String type;
}
